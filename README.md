# Zoom Meetings Project

[![TravisCI](https://travis-ci.org/phpexpertsinc/skeleton.svg?branch=master)](https://travis-ci.org/phpexpertsinc/skeleton)
[![Maintainability](https://api.codeclimate.com/v1/badges/503cba0c53eb262c947a/maintainability)](https://codeclimate.com/github/phpexpertsinc/SimpleDTO/maintainability)
[![Test Coverage](https://api.codeclimate.com/v1/badges/503cba0c53eb262c947a/test_coverage)](https://codeclimate.com/github/phpexpertsinc/SimpleDTO/test_coverage)

Zoom Meetings is a PHP Experts, Inc., Project meant to ease the creation of new projects.

It strives to conform to the Standard PHP Skeleton (https://github.com/php-pds/skeleton) wherever possible.

Read [**On Structuring PHP Projects**](https://blog.nikolaposa.in.rs/2017/01/16/on-structuring-php-projects/)
for more.

The configurer was inspired by https://www.binpress.com/building-project-skeletons-composer/

## Installation

Via Composer

```bash
composer require phexperts/zoom-meetings
```

## Usage

```php
$zoomAPI = new ZoomApiClient();
$response = $zoomAPI->meeting->create($userUUID);
// Use the `$response->start_url to start the meeting.
// Use the `$response->join_url to join the meeting (give this to the participant).
```

# Use cases

 ✔ Create Zoom meetings.  

## Testing

```bash
phpunit --testdox
```

# Contributors

[Theodore R. Smith](https://www.phpexperts.pro/]) <theodore@phpexperts.pro>  
GPG Fingerprint: 4BF8 2613 1C34 87AC D28F  2AD8 EB24 A91D D612 5690  
CEO: PHP Experts, Inc.

## License

MIT license. Please see the [license file](LICENSE) for more information.

