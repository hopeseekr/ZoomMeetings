<?php declare(strict_types=1);

/**
 * This file is part of Zoom Meetings, a PHP Experts, Inc., Project.
 *
 * Copyright © 2020 PHP Experts, Inc.
 * Author: Theodore R. Smith <theodore@phpexperts.pro>
 *   GPG Fingerprint: 4BF8 2613 1C34 87AC D28F  2AD8 EB24 A91D D612 5690
 *   https://www.phpexperts.pro/
 *   https://github.com/PHPExpertsInc/ZoomMeetings
 *
 * This file is licensed under the MIT License.
 */

namespace PHPExperts\ZoomMeetings;

use PHPExperts\RESTSpeaker\HTTPSpeaker;
use PHPExperts\RESTSpeaker\RESTAuth;
use PHPExperts\RESTSpeaker\RESTSpeaker;
use PHPExperts\ZoomMeetings\internal\MeetingManager;

class ZoomApiClient extends RESTSpeaker
{
    /** @var MeetingManager */
    public $meeting;

    public function __construct(RESTAuth $authStrat = null, string $baseURI = 'https://api.zoom.us/v2/', HTTPSpeaker $http = null)
    {
        if (!$authStrat) {
            $authStrat = new ZoomRESTAuth();
        }

        $this->meeting = new MeetingManager($this);

        parent::__construct($authStrat, $baseURI, $http);
    }
}
