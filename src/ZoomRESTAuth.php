<?php declare(strict_types=1);

/**
 * This file is part of Zoom Meetings, a PHP Experts, Inc., Project.
 *
 * Copyright © 2020 PHP Experts, Inc.
 * Author: Theodore R. Smith <theodore@phpexperts.pro>
 *   GPG Fingerprint: 4BF8 2613 1C34 87AC D28F  2AD8 EB24 A91D D612 5690
 *   https://www.phpexperts.pro/
 *   https://github.com/PHPExpertsInc/ZoomMeetings
 *
 * This file is licensed under the MIT License.
 */

namespace PHPExperts\ZoomMeetings;

use PHPExperts\RESTSpeaker\RESTAuth;
use PHPExperts\RESTSpeaker\RESTSpeaker;

class ZoomRESTAuth extends RESTAuth
{
    public function __construct(string $authStratMode = null, RESTSpeaker $apiClient = null)
    {
        if (!$authStratMode) {
            $authStratMode = self::AUTH_MODE_OAUTH2;
        }

        parent::__construct($authStratMode, $apiClient);
    }

    /**
     * @return array<mixed>
     */
    protected function generateOAuth2TokenOptions(): array
    {
        $jwtToken = env('DEV_JWT_TOKEN');

        return [
            'Authorization' => "Bearer $jwtToken",
        ];
    }

    /**
     * @return array<mixed>
     */
    protected function generatePasskeyOptions(): array
    {
        throw new \LogicException('Not implemented.');
    }
}
