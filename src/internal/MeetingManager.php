<?php declare(strict_types=1);

/**
 * This file is part of Zoom Meetings, a PHP Experts, Inc., Project.
 *
 * Copyright © 2020 PHP Experts, Inc.
 * Author: Theodore R. Smith <theodore@phpexperts.pro>
 *   GPG Fingerprint: 4BF8 2613 1C34 87AC D28F  2AD8 EB24 A91D D612 5690
 *   https://www.phpexperts.pro/
 *   https://github.com/PHPExpertsInc/ZoomMeetings
 *
 * This file is licensed under the MIT License.
 */

namespace PHPExperts\ZoomMeetings\internal;

use Carbon\Carbon;
use PHPExperts\ZoomMeetings\ZoomApiClient;

/**
 * @internal
 */
class MeetingManager
{
    /** @var ZoomApiClient */
    public $api;

    public function __construct(ZoomApiClient $api)
    {
        $this->api = $api;
    }

    public function createMeeting(string $userUUID)
    {
        $response = $this->api->post("users/$userUUID/meetings", [
            'topic'      => 'string',
            'type'       => 2,
            'start_time' => Carbon::now(),
            'duration'   => '60',
            'timezone'   => 'CST6CDT',
            'agenda'     => 'Test',
            'settings'   => [
                'host_video'                     => true,
                'participant_video'              => true,
                'join_before_host'               => true,
                'mute_upon_entry'                => true,
                'watermark'                      => false,
                'use_pmi'                        => false,
                'approval_type'                  => 0,
                'enforce_login'                  => false,
                'registrants_email_notification' => false,
            ],
        ]);

        return $response;
    }
}
